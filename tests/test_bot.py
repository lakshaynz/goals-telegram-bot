import requests
import os
import pytest
TELEGRAM_BOT_API_KEY = os.environ.get('TELEGRAM_BOT_API_KEY')

from main import handle_incoming_message

def test_bot_get_me():
    response = requests.get(f'https://api.telegram.org/bot{TELEGRAM_BOT_API_KEY}/getMe')
    assert response.status_code == 200

def test_when_user_sends_hi_bot_replies_hi():
    user_message = {}
    user_message["message_text"]  = "hi"
    assert handle_incoming_message(user_message) == "hi!"
